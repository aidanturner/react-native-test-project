import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import reducers from "./reducers";

/**
 * Logs all actions and states after they are dispatched.
 */
const logger = store => next => action => {
  let result = next(action);
  return result;
};

const store = createStore(
  reducers,
  applyMiddleware(thunk, logger)
);

export default store;
