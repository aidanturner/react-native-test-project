import { GET_BREEDS_LIST } from "../actions/types";

const defaultState = {
    breeds: null
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case GET_BREEDS_LIST: {
            return {
                ...state,
                breeds: action.breeds
            };
        }

        default:
            return state;
    };
};
