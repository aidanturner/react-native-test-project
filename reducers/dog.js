import { SAVE_CHOSEN_DOG } from "../actions/types";

const defaultState = {
  url: null,
};

export default function reducer(state = defaultState, action) {
  switch (action.type) {
    case SAVE_CHOSEN_DOG: {
      return {
        ...state,
        url: action.url
      };
    }

    default:
      return state;
  };
};
