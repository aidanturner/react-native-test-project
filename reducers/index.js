import listReducer from "./list";
import dogReducer from "./dog";
import { combineReducers } from "redux";

export default combineReducers({
    listReducer,
    dogReducer
})