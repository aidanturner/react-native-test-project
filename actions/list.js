import { GET_BREEDS_LIST } from "./types";

export const saveBreedsList = (breeds) => ({
  type: GET_BREEDS_LIST,
  breeds
});

export const get_breeds_list = () => {
  return (dispatch, getState) => {
    fetch("https://dog.ceo/api/breeds/list/all", {
      method: "GET"
    }).then(response => {
      response.json()
        .then(responseJSON => {
          dispatch(saveBreedsList(responseJSON.message));
        })
    })
  };
};
