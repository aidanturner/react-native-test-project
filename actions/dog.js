import { SAVE_CHOSEN_DOG } from "./types";

export const saveChosenDog = (breed, url) => ({
  type: SAVE_CHOSEN_DOG,
  url
});

export const get_dog_image = (breed) => {
  return (dispatch, getState) => {
    fetch("https://dog.ceo/api/breed/" + breed + "/images/random", {
      method: "GET"
    }).then(response => {
      response.json()
        .then(responseJSON => {
          dispatch(saveChosenDog(breed, responseJSON.message));
        })
    })
  };
};
