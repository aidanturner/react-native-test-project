import { Navigation } from 'react-native-navigation';
import Main from "./Main";
import Dog from "./Dog";

export function register_screens(store, Provider) {

  Navigation.registerComponent(
    'inkind.test.Main',
    () => Main,
    store,
    Provider
  );

  Navigation.registerComponent(
    'inkind.test.Dog',
    () => Dog,
    store,
    Provider
  );

};
