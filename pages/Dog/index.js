import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View
} from 'react-native';
import { connect } from "react-redux";
import { get_dog_image } from "../../actions/dog";

class Dog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            url: ''
        };
    }

	componentWillMount(props) {
		this.props.dispatch(get_dog_image(this.props.breed));
    }
    
    componentWillReceiveProps(nextProps) {
        this.setState({
            url: nextProps.dog_url
        });
    }

    render() {
        return (
            <View style={styles.dogWrapper}>
                {
                    this.state.url !== '' && 
                    <Image
                        style={styles.dogImage}
                        source={{uri: this.state.url}}
                    />
                }

                <Text style={styles.label}>{"Look at the dog!"}</Text>
                <Text style={styles.label}>Breed: {this.props.breed}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    dogWrapper: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        width: '100%',
        backgroundColor: '#2d324c',
        paddingTop: 100
    },
    dogImage: {
        width: 200,
        height: 200,
        marginBottom: 50
    },
    label: {
        color: "#fff",
        fontSize: 20
    }
});
  
const mapStateToProps = state => {
    return {
        dog_url: state.dogReducer.url
    };
};
  
export default connect(mapStateToProps)(Dog);
  