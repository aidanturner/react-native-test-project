import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View,
  ScrollView
} from 'react-native';
import { connect } from "react-redux";
import { List, ListItem } from "react-native-elements";
import { get_breeds_list } from "../../actions/list";

class Main extends Component {

	componentWillMount(props) {
		this.props.dispatch(get_breeds_list());
  }

  render() {
    const breeds = this.props.breeds || {};
    const { navigator } = this.props;

    return (
			<ScrollView style={styles.mainWrapper}>
        <List containerStyle={{marginBottom: 20}}>
          {
            Object.keys(breeds).map((key, index) => {
              return (
                <ListItem 
                  key={index}
                  title={key}     
                  onPress={() => {
                    navigator.push({
                      screen: 'inkind.test.Dog',
                      passProps: {
                        breed: key
                      }
                    })
                  }}
                />
              )
            })
          }
        </List>
			</ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  mainWrapper: {
    marginTop: 20,
    marginBottom: 20
  }
});

const mapStateToProps = state => {
	return {
    breeds: state.listReducer.breeds,
	};
};

export default connect(mapStateToProps)(Main);
